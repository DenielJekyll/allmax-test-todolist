import {combineReducers} from 'redux';
import {routerReducer as router} from 'react-router-redux';
import {reducer as form} from 'redux-form';
import tasksReducer, {moduleName as tasksModule} from '../ducks/tasks';

export default combineReducers({
   router,
   form,
    [tasksModule]: tasksReducer,
});
