import React, {Fragment} from 'react';
import TaskForm from './TaskForm';
import { Modal } from 'antd';

class TaskCard extends React.Component {
    
    render() {
        return <Modal
            title="Basic Modal"
            visible={this.props.open}
            onOk={this.props.handleClose}
            onCancel={this.props.handleClose}
        >
            {this.getBody()}
        </Modal>
    };

    getBody = function () {
        const {task} = this.props;
        if (!task) return ''
            return (
                <Fragment>
                    <TaskForm onSubmit={this.editHandler} task={task}/>
                    <button onClick={this.deleteHandler}>delete</button>
                    {task.finished ? null : <button onClick={this.finishHandler}>done</button>}
                </Fragment>
            );
    };
    editHandler = ({description, dateTo, priority}) => {
        return this.props.editTaskHandler({description, dateTo, priority, taskId: this.props.task.uid})
    };
    finishHandler = () => {
        this.props.finishTaskHandler(this.props.task.uid);
        this.props.handleClose()
    }
    deleteHandler = () => {
        this.props.removeTaskHandler(this.props.task.uid);
        this.props.handleClose()
    }
}

export default TaskCard;
