import React from 'react';
import {Table, Column} from 'react-virtualized';
import 'react-virtualized/styles.css'
import {connect} from 'react-redux';
import {fetchAll, taskListSelector, addTask, removeTask, finishTask, editTask} from '../../ducks/tasks';
import TaskCard from './TaskCard';
import TaskForm from './TaskForm';
import PriorityChooseForm from './PriorityChooseForm';


class TaskList extends React.Component {
    state = {
        open: false,
        clickedTask: null
    };
    componentDidMount(){
        const {fetchAll} = this.props;
        fetchAll();
    }

    render() {
        const {tasks} = this.props;
        const style = {
            border: "1px solid black",
            marginTop: '20px',
            width: 400
        };
        return (
            <div>
                <TaskForm onSubmit={this.handleAddTask} />
                <Table
                    rowStyle={this.rowStyler}
                    style={style}
                    rowCount={tasks.length}
                    rowGetter={this.rowGetter}
                    overscanRowCount={5}
                    width={400}
                    height={400}
                    rowHeight={50}
                    headerHeight={50}
                    onRowClick={this.handleOpen}
                >
                    <Column dataKey="dateTo" label="Date to" width={150}/>
                    <Column dataKey="description" label="Description" width={400}/>
                </Table>
                <PriorityChooseForm />
                <TaskCard
                    handleClose={this.handleClose}
                    open={this.state.open}
                    editTaskHandler={this.props.editTask}
                    finishTaskHandler={this.props.finishTask}
                    removeTaskHandler={this.props.removeTask}
                    task={this.state.clickedTask}
                />
            </div>
        );
    };

    handleClose = () => {
        this.setState({
            open: false
        })
    };

    handleOpen = (ev) => {
        this.setState({
            open: true,
            clickedTask: this.props.tasks[ev.index]
        })
    };

    rowGetter = ({
        index,
    }) => {
        const task = this.props.tasks[index];
        const dateOptions = {
            month: 'long',
            day: 'numeric',
            timezone: 'UTC',
        };
        return {
            dateTo: task.dateTo.toLocaleString("en-US", dateOptions),
            description: task.description,
            uid: task.uid
        };
    };

    rowStyler = ({
        index,
    }) => {
        const task = this.props.tasks[index];
        return task ? {
            backgroundColor: task.finished ? 'green' : task.dateTo < Date.now() ? 'red' : 'white',
            color: (task.priority === 'high') ? 'orange' : (task.priority === 'mid') ? 'blue' : 'black',
            border: "1px solid black"
        } : {};
    };

    handleAddTask = ({description, dateTo, priority}) => this.props.addTask({description, dateTo, priority})
}

export default connect((state) => ({
    tasks: taskListSelector(state),
}), {fetchAll, addTask, removeTask, finishTask, editTask})(TaskList);
